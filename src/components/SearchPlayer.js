import { Component } from 'react';
import Text from './Text';
import Password from './Password';
import Lvl from './Lvl';
import Experience from './Experience';

class CreatePlayer extends Component {
  state = {
    username: '',
    password: '',
    lvl: '',
    experience: '',

    showSummary: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showSummary: false,
    });
  };

  handleShowData = () => {
    this.setState({ showSummary: true });
  };

  render() {
    return (
      <div style={{ marginTop: '100px' }}>
        <div className="container">
          <div className="row justify-content-center ">
            <div className="col-md-4 ">
              <div className="card">
                <div className="card-body">
                  <form>
                    <h3>Search Player Form</h3>
                    <div className="form-group">
                      <label>Username :</label>
                      <Text input type="text" name="username" className="form-control" placeholder="Username" value={this.state.username} onChange={this.handleFieldChange} />
                    </div>
                    <div className="form-group">
                      <label>Password :</label>
                      <Password input type="password" name="password" className="form-control" placeholder="Password" value={this.state.password} onChange={this.handleFieldChange} />
                    </div>
                    <div className="form-group">
                      <label>Level :</label>
                      <Lvl input type="text" name="lvl" className="form-control" placeholder="Level" value={this.state.lvl} onChange={this.handleFieldChange} />
                    </div>
                    <div className="form-group">
                      <label>Experience :</label>
                      <Experience input type="text" name="experience" className="form-control" placeholder="Experience" value={this.state.experience} onChange={this.handleFieldChange} />
                    </div>
                  </form>
                  <button type="submit" className="btn btn-success btn-block mt-4" onClick={this.handleShowData}>
                    Submit
                  </button>
                  {this.state.showSummary && (
                    <ul class="list-group mt-4">
                      <li class="list-group-item">Username : {this.state.username}</li>
                      <li class="list-group-item">Password : {this.state.password}</li>
                      <li class="list-group-item">Level : {this.state.lvl}</li>
                      <li class="list-group-item">Experience : {this.state.experience}</li>
                    </ul>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreatePlayer;
