import { Component } from 'react';
import Text from './Text';

class CreatePlayer extends Component {
  state = {
    username: 'Faza',
    password: '',
    showSummary: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showSummary: false,
    });
  };

  handleShowData = () => {
    this.setState({ showSummary: true });
  };

  render() {
    return (
      <div style={{ marginTop: '100px' }}>
        <div className="container">
          <div className="row justify-content-center ">
            <div className="col-md-4 ">
              <div className="card">
                <div className="card-body">
                  <form>
                    <h3>Update Player Form</h3>
                    <div className="form-group">
                      <label>Username :</label>
                      <Text input type="text" name="username" className="form-control" placeholder="Username" value={this.state.username} onChange={this.handleFieldChange} />
                    </div>
                  </form>
                  <button type="submit" className="btn btn-success btn-block mt-4" onClick={this.handleShowData}>
                    Show
                  </button>
                  {this.state.showSummary && (
                    <ul class="list-group mt-2">
                      <li class="list-group-item">Username : {this.state.username}</li>
                    </ul>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreatePlayer;
