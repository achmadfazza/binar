import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Beranda from './Beranda';
import CreatePlayer from './CreatePlayer';
import UpdatePlayer from './UpdatePlayer';
import SearchPlayer from './SearchPlayer';

const Utama = () => (
  <Switch>
    <Route exact path="/" component={Beranda} />
    <Route exact path="/Beranda" component={Beranda} />
    <Route path="/CreatePlayer" component={CreatePlayer} />
    <Route path="/UpdatePlayer" component={UpdatePlayer} />
    <Route path="/SearchPlayer" component={SearchPlayer} />
  </Switch>
);

export default Utama;
