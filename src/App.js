import React, { Component } from 'react';
import Utama from './components/Utama';
import { Link } from 'react-router-dom';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <hr />
        <Link to="Beranda">Beranda</Link> |<Link to="CreatePlayer">Create Player</Link> |<Link to="UpdatePlayer">Updates Player</Link> |<Link to="SearchPlayer">Search Player</Link> | <hr />
        <p>
          <Utama />
        </p>
      </div>
    );
  }
}

export default App;
